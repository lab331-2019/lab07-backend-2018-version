package se331.lab.rest.controller;

import org.springframework.stereotype.Controller;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import se331.lab.rest.entity.Student;

import java.util.ArrayList;
import java.util.List;

@Controller
public class StudentController {
    List<Student> students;
    public StudentController() {
        this.students = new ArrayList<>();
        this.students.add(Student.builder()
                .id(1L)
                .studentId("SE-001")
                .name("Nagao")
                .surname("Kagetora")
                .gpa(3.66)
                .image("http://34.229.52.202:8190/images/nagao.jpg")
                .penAmount(8)
                .description("Best Waifu Lancer")
                .build());
        this.students.add(Student.builder()
                .id(2L)
                .studentId("SE-002")
                .name("Okita")
                .surname("Souji")
                .gpa(3.1)
                .image("http://34.229.52.202:8190/images/commitOkita.png")
                .penAmount(8)
                .description("Best Waifu Saber")
                .build());
        this.students.add(Student.builder()
                .id(3L)
                .studentId("SE-003")
                .name("Keanu")
                .surname("Reeves")
                .gpa(3.9)
                .image("http://34.229.52.202:8190/images/animeReeves.jpg")
                .penAmount(666)
                .description("He has a city yo burn")
                .build());

    }

    @GetMapping("/students")
    public ResponseEntity getAllStudent() {
        return ResponseEntity.ok(students);
    }
    @GetMapping("/students/{id}")
    public ResponseEntity getStudentById(@PathVariable("id")Long id) {
        return ResponseEntity.ok(students.get(Math.toIntExact(id-1)));
    }
    @PostMapping("/students")
    public ResponseEntity saveStudent(@RequestBody Student student) {
        student.setId((long) this.students.size());
        this.students.add(student);
        return ResponseEntity.ok(student);
    }

}
